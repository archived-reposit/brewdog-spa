#+TITLE: Notes

* Inspiration

https://dev.to/stroemdev/make-fetch-better-and-your-api-request-methods-easier-to-implement-e9i

http://developerlife.com/2019/07/06/starter-project-typescript-karma-jasmine-webpack/

https://www.30secondsofcode.org/blog/s/javascript-modify-url-without-reload

https://blog.skay.dev/custom-spa-router-vanillajs

https://www.willtaylor.blog/client-side-routing-in-vanilla-js/


* alternatives to punkapi
since it seems discontinued

https://dev.to/justaashir/fill-up-your-portfolio-with-meaningful-projects-in-2020-use-these-apis-2518

https://restcountries.eu/

https://api.nasa.gov/

https://github.com/r-spacex/SpaceX-API


* router

when loaded, check the url, parse the path (start with only one), match against known routes, fetch corresponding html-template.
class or function?

** regex
/^\/([\w|-]*)/gi
Should allow - in names, and only captures one path / in the first "group"
pathname seems to return / even if it is not displayed (example.com returns /)

* templates
https://dev.to/pluralsight/vanilla-javascript-and-html-no-frameworks-no-libraries-no-problem-2n99
pass a template to beer class for rendering?

* autocomplete
looks like data-list can not be used with js.
** requirements
By using the search bar it should be possible to filter beers by name. The search bar should allow for
a dropdown with a list of suggestions based on the five most recent searches.

"The application should remember the five most recent search terms even after
reloading the page.
The suggestion dropdown items should be filtered by the current search string in the
search bar."
