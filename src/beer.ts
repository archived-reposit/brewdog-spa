import { TemplateType } from './router';

class Beer {
  id: number;
  name: string;
  tagline: string;
  first_brewed?: string;
  description?: string;
  image_url: string;
  public render?( template: HTMLTemplateElement, templateType: TemplateType ) {
    const templateContent = document.importNode( template.content, true);
    
    if ( templateType.id === 'Root' ) {
      const beerNameElement = templateContent.querySelector('.name')
      beerNameElement.textContent = this.name;
      (beerNameElement as HTMLAnchorElement).href = '/' + this.name;

    } else if ( templateType.id === 'BeerDetails' ) {
      const beerNameElement = templateContent.querySelector('.name')
      beerNameElement.firstChild.textContent = this.name;
      (beerNameElement.firstChild as HTMLAnchorElement).href = '/' + this.name;

      templateContent.querySelector('.description').textContent = this.tagline;
      (templateContent.querySelector('.image') as HTMLImageElement).src = this.image_url;
    }
    return templateContent;
    //console.log(template);
    // let el = document.createElement("li");
    // el.innerHTML = this.name;
    // return el;
  }
  constructor( beer: Beer ){
    Object.assign(this, beer);
    console.info(this)
  }
}

export { Beer };
