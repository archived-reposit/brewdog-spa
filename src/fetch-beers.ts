import { Beer } from "./beer";
const baseUrlForQuery = "https://api.punkapi.com/v2/beers";

export async function getAllBeers(): Promise<Beer[]> {
  return apiCall();
}

export async function getBeersWithName(searchString: string): Promise<Beer[]> {
  return apiCall(searchString);
  }

async function apiCall(searchString?: string): Promise<Beer[]> {
  let urlForQuery = baseUrlForQuery;
  if (searchString) {
    urlForQuery += "?beer_name=" + searchString;
  }
  const resultFromApiCall = await fetch(urlForQuery);
  if (resultFromApiCall.ok){
    return parseBeers(await resultFromApiCall.json());
  } else {
    alert(resultFromApiCall.statusText);
  }
}

export function parseBeers(responseFromApi: any[]): Beer[] {
  let beers: Beer[] = [];
  let beersWithAllData: any[] = responseFromApi; // JSON.parse(responseFromApi);
  beersWithAllData.forEach(data => {
    let beer: Beer = {
      id: (data as any).id,
      name: (data as any).name,
      tagline: (data as any).tagline,
      image_url: (data as any).image_url
    };
    beers.push(beer);
  });
  return beers;
}
