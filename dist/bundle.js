/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/fetch-beers.ts":
/*!****************************!*\
  !*** ./src/fetch-beers.ts ***!
  \****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "getAllBeers": () => (/* binding */ getAllBeers),
/* harmony export */   "getBeersWithName": () => (/* binding */ getBeersWithName),
/* harmony export */   "parseBeers": () => (/* binding */ parseBeers)
/* harmony export */ });
const baseUrlForQuery = "https://api.punkapi.com/v2/beers";
async function getAllBeers() {
    return apiCall();
}
async function getBeersWithName(searchString) {
    return apiCall(searchString);
}
async function apiCall(searchString) {
    let urlForQuery = baseUrlForQuery;
    if (searchString) {
        urlForQuery += "?beer_name=" + searchString;
    }
    alert(urlForQuery);
    const resultFromApiCall = await fetch(urlForQuery);
    if (resultFromApiCall.ok)
        return parseBeers(await resultFromApiCall.json());
}
function parseBeers(responseFromApi) {
    let beers = [];
    let beersWithAllData = responseFromApi; // JSON.parse(responseFromApi);
    beersWithAllData.forEach(data => {
        let beer = {
            id: data.id,
            name: data.name,
            tagline: data.tagline,
            image_url: data.image_url
        };
        beers.push(beer);
    });
    return beers;
}


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
/*!**********************!*\
  !*** ./src/index.ts ***!
  \**********************/
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _fetch_beers__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./fetch-beers */ "./src/fetch-beers.ts");

// let beers: Beer[] = [];
// getAllBeers().then(beersFetched => {
//   // beers = beersFetched;
//   renderBeers(beersFetched);
// });
function searchBeers(searchString) {
    (0,_fetch_beers__WEBPACK_IMPORTED_MODULE_0__.getBeersWithName)(searchString).then(beersFetched => {
        renderBeers(beersFetched);
    });
}
function renderBeers(beers) {
    beers.forEach(beer => {
        console.log(beer.name);
        let b = document.createElement("li");
        b.innerHTML = beer.name;
        document.querySelector("#beerlist").appendChild(b);
    });
}
window.onload = function () {
    // document.querySelector("#searchform").addEventListener('onsubmit', searchBeers)
    // const form =  document.getElementById('search-form');
    const form = document.querySelector('#search-form');
    form.onsubmit = () => {
        const formData = new FormData(form);
        const searchString = formData.get('beer-searched');
        searchBeers(searchString);
        return false;
    };
};

})();

/******/ })()
;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly90cy10ZW1wbGF0ZS8uL3NyYy9mZXRjaC1iZWVycy50cyIsIndlYnBhY2s6Ly90cy10ZW1wbGF0ZS93ZWJwYWNrL2Jvb3RzdHJhcCIsIndlYnBhY2s6Ly90cy10ZW1wbGF0ZS93ZWJwYWNrL3J1bnRpbWUvZGVmaW5lIHByb3BlcnR5IGdldHRlcnMiLCJ3ZWJwYWNrOi8vdHMtdGVtcGxhdGUvd2VicGFjay9ydW50aW1lL2hhc093blByb3BlcnR5IHNob3J0aGFuZCIsIndlYnBhY2s6Ly90cy10ZW1wbGF0ZS93ZWJwYWNrL3J1bnRpbWUvbWFrZSBuYW1lc3BhY2Ugb2JqZWN0Iiwid2VicGFjazovL3RzLXRlbXBsYXRlLy4vc3JjL2luZGV4LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDQSxNQUFNLGVBQWUsR0FBRyxrQ0FBa0MsQ0FBQztBQUVwRCxLQUFLLFVBQVUsV0FBVztJQUMvQixPQUFPLE9BQU8sRUFBRSxDQUFDO0FBQ25CLENBQUM7QUFFTSxLQUFLLFVBQVUsZ0JBQWdCLENBQUMsWUFBb0I7SUFDekQsT0FBTyxPQUFPLENBQUMsWUFBWSxDQUFDLENBQUM7QUFDL0IsQ0FBQztBQUVELEtBQUssVUFBVSxPQUFPLENBQUMsWUFBcUI7SUFDMUMsSUFBSSxXQUFXLEdBQUcsZUFBZSxDQUFDO0lBQ2xDLElBQUksWUFBWSxFQUFFO1FBQ2hCLFdBQVcsSUFBSSxhQUFhLEdBQUcsWUFBWSxDQUFDO0tBQzdDO0lBQ0QsS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUFDO0lBQ25CLE1BQU0saUJBQWlCLEdBQUcsTUFBTSxLQUFLLENBQUMsV0FBVyxDQUFDLENBQUM7SUFDbkQsSUFBSSxpQkFBaUIsQ0FBQyxFQUFFO1FBQUUsT0FBTyxVQUFVLENBQUMsTUFBTSxpQkFBaUIsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDO0FBQzlFLENBQUM7QUFFTSxTQUFTLFVBQVUsQ0FBQyxlQUFzQjtJQUMvQyxJQUFJLEtBQUssR0FBVyxFQUFFLENBQUM7SUFDdkIsSUFBSSxnQkFBZ0IsR0FBVSxlQUFlLENBQUMsQ0FBQywrQkFBK0I7SUFDOUUsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFO1FBQzlCLElBQUksSUFBSSxHQUFTO1lBQ2YsRUFBRSxFQUFHLElBQVksQ0FBQyxFQUFFO1lBQ3BCLElBQUksRUFBRyxJQUFZLENBQUMsSUFBSTtZQUN4QixPQUFPLEVBQUcsSUFBWSxDQUFDLE9BQU87WUFDOUIsU0FBUyxFQUFHLElBQVksQ0FBQyxTQUFTO1NBQ25DLENBQUM7UUFDRixLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ25CLENBQUMsQ0FBQyxDQUFDO0lBQ0gsT0FBTyxLQUFLLENBQUM7QUFDZixDQUFDOzs7Ozs7O1VDbENEO1VBQ0E7O1VBRUE7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7O1VBRUE7VUFDQTs7VUFFQTtVQUNBO1VBQ0E7Ozs7O1dDdEJBO1dBQ0E7V0FDQTtXQUNBO1dBQ0Esd0NBQXdDLHlDQUF5QztXQUNqRjtXQUNBO1dBQ0EsRTs7Ozs7V0NQQSx3Rjs7Ozs7V0NBQTtXQUNBO1dBQ0E7V0FDQSxzREFBc0Qsa0JBQWtCO1dBQ3hFO1dBQ0EsK0NBQStDLGNBQWM7V0FDN0QsRTs7Ozs7Ozs7Ozs7O0FDTDhEO0FBRTlELDBCQUEwQjtBQUUxQix1Q0FBdUM7QUFDdkMsNkJBQTZCO0FBQzdCLCtCQUErQjtBQUMvQixNQUFNO0FBRU4sU0FBUyxXQUFXLENBQUMsWUFBbUI7SUFDdEMsOERBQWdCLENBQUMsWUFBWSxDQUFDLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFO1FBQ2pELFdBQVcsQ0FBQyxZQUFZLENBQUMsQ0FBQztJQUM1QixDQUFDLENBQUMsQ0FBQztBQUNMLENBQUM7QUFHRCxTQUFTLFdBQVcsQ0FBQyxLQUFhO0lBQ2hDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7UUFDbkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDdkIsSUFBSSxDQUFDLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNyQyxDQUFDLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7UUFDeEIsUUFBUSxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDckQsQ0FBQyxDQUFDLENBQUM7QUFDTCxDQUFDO0FBRUQsTUFBTSxDQUFDLE1BQU0sR0FBRztJQUNkLGtGQUFrRjtJQUNsRix3REFBd0Q7SUFDeEQsTUFBTSxJQUFJLEdBQW9CLFFBQVEsQ0FBQyxhQUFhLENBQUMsY0FBYyxDQUFDLENBQUM7SUFDckUsSUFBSSxDQUFDLFFBQVEsR0FBRyxHQUFHLEVBQUU7UUFDbkIsTUFBTSxRQUFRLEdBQUcsSUFBSSxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDcEMsTUFBTSxZQUFZLEdBQUcsUUFBUSxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQVcsQ0FBQztRQUM3RCxXQUFXLENBQUUsWUFBWSxDQUFFLENBQUM7UUFDNUIsT0FBTyxLQUFLLENBQUM7SUFDZixDQUFDO0FBQ0gsQ0FBQyIsImZpbGUiOiJidW5kbGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBCZWVyIH0gZnJvbSBcIi4vYmVlclwiO1xuY29uc3QgYmFzZVVybEZvclF1ZXJ5ID0gXCJodHRwczovL2FwaS5wdW5rYXBpLmNvbS92Mi9iZWVyc1wiO1xuXG5leHBvcnQgYXN5bmMgZnVuY3Rpb24gZ2V0QWxsQmVlcnMoKTogUHJvbWlzZTxCZWVyW10+IHtcbiAgcmV0dXJuIGFwaUNhbGwoKTtcbn1cblxuZXhwb3J0IGFzeW5jIGZ1bmN0aW9uIGdldEJlZXJzV2l0aE5hbWUoc2VhcmNoU3RyaW5nOiBzdHJpbmcpOiBQcm9taXNlPEJlZXJbXT4ge1xuICByZXR1cm4gYXBpQ2FsbChzZWFyY2hTdHJpbmcpO1xufVxuXG5hc3luYyBmdW5jdGlvbiBhcGlDYWxsKHNlYXJjaFN0cmluZz86IHN0cmluZykge1xuICBsZXQgdXJsRm9yUXVlcnkgPSBiYXNlVXJsRm9yUXVlcnk7XG4gIGlmIChzZWFyY2hTdHJpbmcpIHtcbiAgICB1cmxGb3JRdWVyeSArPSBcIj9iZWVyX25hbWU9XCIgKyBzZWFyY2hTdHJpbmc7XG4gIH1cbiAgYWxlcnQodXJsRm9yUXVlcnkpO1xuICBjb25zdCByZXN1bHRGcm9tQXBpQ2FsbCA9IGF3YWl0IGZldGNoKHVybEZvclF1ZXJ5KTtcbiAgaWYgKHJlc3VsdEZyb21BcGlDYWxsLm9rKSByZXR1cm4gcGFyc2VCZWVycyhhd2FpdCByZXN1bHRGcm9tQXBpQ2FsbC5qc29uKCkpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gcGFyc2VCZWVycyhyZXNwb25zZUZyb21BcGk6IGFueVtdKTogQmVlcltdIHtcbiAgbGV0IGJlZXJzOiBCZWVyW10gPSBbXTtcbiAgbGV0IGJlZXJzV2l0aEFsbERhdGE6IGFueVtdID0gcmVzcG9uc2VGcm9tQXBpOyAvLyBKU09OLnBhcnNlKHJlc3BvbnNlRnJvbUFwaSk7XG4gIGJlZXJzV2l0aEFsbERhdGEuZm9yRWFjaChkYXRhID0+IHtcbiAgICBsZXQgYmVlcjogQmVlciA9IHtcbiAgICAgIGlkOiAoZGF0YSBhcyBhbnkpLmlkLFxuICAgICAgbmFtZTogKGRhdGEgYXMgYW55KS5uYW1lLFxuICAgICAgdGFnbGluZTogKGRhdGEgYXMgYW55KS50YWdsaW5lLFxuICAgICAgaW1hZ2VfdXJsOiAoZGF0YSBhcyBhbnkpLmltYWdlX3VybFxuICAgIH07XG4gICAgYmVlcnMucHVzaChiZWVyKTtcbiAgfSk7XG4gIHJldHVybiBiZWVycztcbn1cbiIsIi8vIFRoZSBtb2R1bGUgY2FjaGVcbnZhciBfX3dlYnBhY2tfbW9kdWxlX2NhY2hlX18gPSB7fTtcblxuLy8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbmZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG5cdHZhciBjYWNoZWRNb2R1bGUgPSBfX3dlYnBhY2tfbW9kdWxlX2NhY2hlX19bbW9kdWxlSWRdO1xuXHRpZiAoY2FjaGVkTW9kdWxlICE9PSB1bmRlZmluZWQpIHtcblx0XHRyZXR1cm4gY2FjaGVkTW9kdWxlLmV4cG9ydHM7XG5cdH1cblx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcblx0dmFyIG1vZHVsZSA9IF9fd2VicGFja19tb2R1bGVfY2FjaGVfX1ttb2R1bGVJZF0gPSB7XG5cdFx0Ly8gbm8gbW9kdWxlLmlkIG5lZWRlZFxuXHRcdC8vIG5vIG1vZHVsZS5sb2FkZWQgbmVlZGVkXG5cdFx0ZXhwb3J0czoge31cblx0fTtcblxuXHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cblx0X193ZWJwYWNrX21vZHVsZXNfX1ttb2R1bGVJZF0obW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cblx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcblx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xufVxuXG4iLCIvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9ucyBmb3IgaGFybW9ueSBleHBvcnRzXG5fX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSAoZXhwb3J0cywgZGVmaW5pdGlvbikgPT4ge1xuXHRmb3IodmFyIGtleSBpbiBkZWZpbml0aW9uKSB7XG5cdFx0aWYoX193ZWJwYWNrX3JlcXVpcmVfXy5vKGRlZmluaXRpb24sIGtleSkgJiYgIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBrZXkpKSB7XG5cdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywga2V5LCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZGVmaW5pdGlvbltrZXldIH0pO1xuXHRcdH1cblx0fVxufTsiLCJfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSAob2JqLCBwcm9wKSA9PiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iaiwgcHJvcCkpIiwiLy8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuX193ZWJwYWNrX3JlcXVpcmVfXy5yID0gKGV4cG9ydHMpID0+IHtcblx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG5cdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG5cdH1cblx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbn07IiwiaW1wb3J0IHsgQmVlciB9IGZyb20gXCIuL2JlZXJcIjtcbmltcG9ydCB7IGdldEFsbEJlZXJzLCBnZXRCZWVyc1dpdGhOYW1lIH0gZnJvbSBcIi4vZmV0Y2gtYmVlcnNcIjtcblxuLy8gbGV0IGJlZXJzOiBCZWVyW10gPSBbXTtcblxuLy8gZ2V0QWxsQmVlcnMoKS50aGVuKGJlZXJzRmV0Y2hlZCA9PiB7XG4vLyAgIC8vIGJlZXJzID0gYmVlcnNGZXRjaGVkO1xuLy8gICByZW5kZXJCZWVycyhiZWVyc0ZldGNoZWQpO1xuLy8gfSk7XG5cbmZ1bmN0aW9uIHNlYXJjaEJlZXJzKHNlYXJjaFN0cmluZzpzdHJpbmcpIHtcbiAgZ2V0QmVlcnNXaXRoTmFtZShzZWFyY2hTdHJpbmcpLnRoZW4oYmVlcnNGZXRjaGVkID0+IHtcbiAgICByZW5kZXJCZWVycyhiZWVyc0ZldGNoZWQpO1xuICB9KTtcbn1cblxuXG5mdW5jdGlvbiByZW5kZXJCZWVycyhiZWVyczogQmVlcltdKSB7XG4gIGJlZXJzLmZvckVhY2goYmVlciA9PiB7XG4gICAgY29uc29sZS5sb2coYmVlci5uYW1lKTtcbiAgICBsZXQgYiA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJsaVwiKTtcbiAgICBiLmlubmVySFRNTCA9IGJlZXIubmFtZTtcbiAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI2JlZXJsaXN0XCIpLmFwcGVuZENoaWxkKGIpO1xuICB9KTtcbn1cblxud2luZG93Lm9ubG9hZCA9IGZ1bmN0aW9uKCkge1xuICAvLyBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI3NlYXJjaGZvcm1cIikuYWRkRXZlbnRMaXN0ZW5lcignb25zdWJtaXQnLCBzZWFyY2hCZWVycylcbiAgLy8gY29uc3QgZm9ybSA9ICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnc2VhcmNoLWZvcm0nKTtcbiAgY29uc3QgZm9ybTogSFRNTEZvcm1FbGVtZW50ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignI3NlYXJjaC1mb3JtJyk7XG4gIGZvcm0ub25zdWJtaXQgPSAoKSA9PiB7XG4gICAgY29uc3QgZm9ybURhdGEgPSBuZXcgRm9ybURhdGEoZm9ybSk7XG4gICAgY29uc3Qgc2VhcmNoU3RyaW5nID0gZm9ybURhdGEuZ2V0KCdiZWVyLXNlYXJjaGVkJykgYXMgc3RyaW5nO1xuICAgIHNlYXJjaEJlZXJzKCBzZWFyY2hTdHJpbmcgKTtcbiAgICByZXR1cm4gZmFsc2U7XG4gIH1cbn1cbiJdLCJzb3VyY2VSb290IjoiIn0=